package az.orient.course.util;

/**
 * Created with IntelliJ IDEA.
 * User: fuadp
 * Date: 4/15/17
 * Time: 6:54 PM
 * To change this template use File | Settings | File Templates.
 */
public final class Constants {

       public static final String GET_STUDENT_LIST = "getStudentList";
       public static final String GET_TEACHER_LIST = "getTeacherList";
       public static final String ADVANCED_SEARCH_STUDENT = "advancedSearchStudent";

}
