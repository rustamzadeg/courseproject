package az.orient.course.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: fuadp
 * Date: 3/18/17
 * Time: 3:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class JdbcUtility {

    public static void close(Connection c,PreparedStatement ps,ResultSet rs) throws Exception{
        if (c!= null) {
            c.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }
}
